Un exemple de dépôt Binder
==========================
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitub.u-bordeaux.fr%2Ffsantos%2Fmybinder_example_r/master?filepath=index.ipynb)

## Enjeu
Nous donnons ici un exemple d'analyse reproductible, déposée sur Gitlab.com, et liée au service MyBinder afin de permettre l'exécution des analyses sur un serveur distant (dont on maîtrise et spécifie précisément l'environnement logiciel).

## D'autres exemples
Voici quelques exemples notables de dépôts comparables, correspondant à de "vraies" publications. Certains exemples utilisent plutôt une exécution distante de Rstudio (au lieu de Jupyter), mais cela reste identique dans la philosophie et le principe.
- [*From noise to knowledge: how randomness generates novel phenomena and reveals information*](https://github.com/cboettig/noise-phenomena)
- [*Neural hierarchical models of ecological populations*](https://github.com/mbjoseph/neuralecology)
- [Un exemple en archéologie](https://github.com/bbartholdy/byoc-starch)